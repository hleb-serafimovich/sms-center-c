#pragma comment (lib,"Ws2_32.lib")
#pragma warning(disable: 4996)

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring> 
#include<iostream>
#include <fstream>
#include <conio.h>
#include <iomanip>
#include <vector>

using namespace std;

void DelSMS(void* client_socket);
void SendSMS(void* client_socket);

void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	int choice;
	bool flag_exit = true;
	WSAData wsData;
	WORD DLLVersion = MAKEWORD(2, 1);   
	if (WSAStartup(DLLVersion, &wsData) != 0) 
	{
		cout << "Error!" << endl;
		system("pause");
		exit(1);
	}

	SOCKADDR_IN addr;  
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1111);
	SOCKET s = socket(AF_INET, SOCK_STREAM, 0);
	if (connect(s, (SOCKADDR *)&addr, sizeof(addr)) != 0)
	{
		cout << "Error" << endl;
		system("pause");
		exit(1);
	}

	while (flag_exit)
	{
		system("cls");
		cout << "  \t\t __________________________________________________________________________________________________________" << endl;
		cout << "  \t\t|" << "\t\t\t\t\t   M A I N   M E N U" << "\t\t\t\t\t   |" << endl;
		cout << "  \t\t|__________________________________________________________________________________________________________|" << endl;
		cout << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  1  |   S E N D   S M S" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "  \t\t _________________________________________________________________________________" << endl;
		cout << "  \t\t|  2  |   D E L E T E   S M S" << "\t\t\t\t\t\t  |" << endl;
		cout << "  \t\t|_____|___________________________________________________________________________|" << endl;
		cout << "\t\t _________________________________" << endl;
		cout << "\t\t| ESC |" << "          E X I T \t  |" << endl;
		cout << "\t\t|_____|___________________________|" << endl;

		choice = _getch();

		switch (choice)
		{
		case '1': SendSMS((void*)s); break;
		case '2': DelSMS((void*)s); break;
		case 27: 
			send(s, "0", strlen("0"), NULL);
			flag_exit = false; break;
		}

	}

	closesocket(s);
	WSACleanup();
}


void DelSMS(void* client_socket)
{
	system("cls");
	char phone[256];
	char choice[2] = "2";
	send((SOCKET)client_socket, choice, strlen(choice), NULL);
	cout << "\n\t\tEnter the phone number: "; ";
	cin >> phone;
	send((SOCKET)client_socket, phone, strlen(phone), NULL);
}



void SendSMS(void* client_socket)
{
	system("cls");
	char phone[256];
	char text[256];
	char choice[2] = "1";
	send((SOCKET)client_socket, choice, strlen(choice), NULL);
	cout << "\n\t\tSend a message with permission to delete [1] ";
	cout << "\n\t\tSend a message without permission to delete [2] ";
	cout << "\n\t\tYour choice: ";
	cin >> choice;
	cout << "\n\t\tEnter the phone number:  ";
	cin >> phone;
	int size = strlen(phone);
	if (choice[0] == '1')
	{
		for (int i = strlen(phone) - 1; i >= 0; i--)
		{
			phone[i + 1] = phone[i];
		}
		phone[0] = 'D';
		phone[size + 1] = '\0';
	}
	send((SOCKET)client_socket, phone, strlen(phone), NULL);
	cout << "\n\t\tEnter your message: ";
	cin >> text;
	send((SOCKET)client_socket, text, strlen(text), NULL);

}
