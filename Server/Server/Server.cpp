#pragma comment (lib,"Ws2_32.lib")
#pragma warning(disable: 4996)


#include <iostream>
#include <winsock2.h>
#include <process.h>    /* _beginthread, _endthread */
#include <time.h>
#include <string>
#include <fstream>
#include <sys/types.h>
#include <vector>


using namespace std;

int NUMBER = 0;
int TIME = 0;
vector<vector<int>> vec(100, vector<int>(100));
string INFO_FILE = "C:/dd/Info.txt"; 
char phone[256], text[256];
clock_t start, finish;
int counter = 0;

void DelSMS(void* client_socket);
void SendSMS(void* client_socket);
void SMSworking(void* client_socket);
void print();

void main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);

	WSAData wsData;
	WORD DLLVersion = MAKEWORD(2, 1);   
	if (WSAStartup(DLLVersion, &wsData) != 0) 
	{
		cout << "Error!" << endl;
		system("pause");
		exit(1);
	}

	SOCKADDR_IN addr;  
	addr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
	addr.sin_family = AF_INET;
	addr.sin_port = htons(1111);

	SOCKET sListen = socket(AF_INET, SOCK_STREAM, 0);
	bind(sListen, (SOCKADDR*)&addr, sizeof(addr));
	listen(sListen, 5);

	SOCKET client_socket;  
	SOCKADDR_IN client_addr; 
	int client_addr_size = sizeof(client_addr);

	while (client_socket = accept(sListen, (sockaddr *)&client_addr, &client_addr_size))
	{
		counter++;
		print();
		cout << "New client\nport - " << ntohs(client_addr.sin_port) << "\ndeskriptor:" << client_socket << "\nIP: " << inet_ntoa(client_addr.sin_addr) << endl;
		_beginthread(SMSworking, 0, (void*)client_socket);
	}

	closesocket(sListen);
	WSACleanup();
}



void DelSMS(void* client_socket)
{
	int rv = recv((SOCKET)client_socket, phone, sizeof(phone), 0);
	phone[rv] = '\0';
	string  tempPhone = phone;
	string  delPhone = "C:/dd/D" + tempPhone + ".txt";
	if (remove(delPhone.c_str()) != 0)
	{
		cout << "SMS to number " << phone << "  can not be deleted " << endl;
	}
	else
	{
		cout << "SMS to number " << phone << " deleted " << endl;
	}
}


void  SendSMS(void* client_socket)
{
	int rv = recv((SOCKET)client_socket, phone, sizeof(phone), 0);  
	phone[rv] = '\0';
	string  tempPhone = phone;

	rv = recv((SOCKET)client_socket, text, sizeof(text), 0);  
	text[rv] = '\0';
	string  tempText = text;

	string  sendSMS = "C:/dd/" + tempPhone + ".txt";
	ofstream write(sendSMS, ios::out);
	write << text;
	write.close();
}


void SMSworking(void* client_socket)
{
	send((SOCKET)client_socket, "SMS center connected!", strlen("SMS center connected!"), NULL);
	bool flag_exit = true;
	char choice[2];

	while (recv((SOCKET)client_socket, choice, sizeof(choice), 0))
	{
		switch (choice[0])
		{
		case '1':  
			start = clock();
			SendSMS(client_socket); 
			break;
		case '2':  
			finish = clock();
			if ((double)(finish - start) / CLOCKS_PER_SEC > 60)
				cout << "Cannot be canceled" << endl;
			else
			{
				DelSMS(client_socket);
			}
			break;
		case '0':  
			counter--;
			print();
			closesocket((SOCKET)client_socket); 
			_endthread(); 
			break;
		}
	}
}



void print()
{
	system("cls");
	cout << "Server receive ready" << endl;
	cout << "clients on the server: " << counter << endl;
}
